<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    const ADMIN_DEFAULT = 1;
    const ADMIN_SUPER = 2;
    protected $table = 'admins';
    protected $fillable = [
        'name', 'email', 'addadmin','password', 'type'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

     protected $casts = [
             'email_verified_at' => 'datetime'
        ];

     public function isSuperAdmin() {
         return $this->type === self::ADMIN_SUPER;
     }
     public function isDefaultAdmin() {
         return $this->type === self::ADMIN_DEFAULT;
     }

}