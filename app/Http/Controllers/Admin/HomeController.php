<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class Home1Controller extends Controller {

    public function __construct()
    {
    }

    public function index() {
        return view('admin.home.index');
    }
}