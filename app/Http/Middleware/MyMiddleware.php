<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Auth;

class MyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null, $redirectTo = '/')
    {
        if(!Auth::guard($guard)->check()) {
            return redirect($redirectTo);
        }
        $admin = Auth::guard($guard)->user();
        //dd($admin); bo comment xem no' dd ra cai gi` ma` hoc ne`
        //sau khi dang nhap xong co duoc thong tin cua admin. gio` ta kiem tra type cho no
        if($admin->isSuperAdmin()) {
            echo('xin chao ban la supper admin');
            return $next($request);
        }
        if($admin->isDefaultAdmin()) {
            echo('xin chao ban la default admin');
            dd('stop stop a hi hi .vào xem code đi mà biết phân quyền nè');
        }
    }
}
